import java.util.List;

/*
 * AlphabetSorter sorts a list of lines in ascending alphabetical order
 */
public class AlphabetSorter{
	//Array holding sorted indexes of lines
	private int sortedIndexes_[];

	//CircularShifter that provides lines
	private CircularShifter circularShifter_;

	public void sort(CircularShifter shifter){
		circularShifter_ = shifter;

		sortedIndexes_ = new int[circularShifter_.getNumberOfLines()];
		for(int i = 0; i < sortedIndexes_.length; i++){
			sortedIndexes_[i] = i;
		}

		MergeSort(0,sortedIndexes_.length-1);
	}


	private void MergeSort(int low, int high){
		if (low < high) {
			// Find where to split the set.
			int mid = (low + high)/2;
			
			// Solve the subproblems.
			MergeSort(low, mid);
			MergeSort(mid + 1, high);
			
			// Combine the solutions.
			Merge(low, mid, high);
		}
	}


	private void Merge(int low, int mid, int high){
		int h = low, i = low, j = mid+1;			// h, i and j are just pointers
		int placeholderArray[] = new int[sortedIndexes_.length];
		
		while ((h <= mid) && (j <= high)) {
			if (circularShifter_.getLineAsString(sortedIndexes_[h])
					.compareTo(circularShifter_.getLineAsString(sortedIndexes_[j])) <= 0) { 
				placeholderArray[i] = sortedIndexes_[h]; 
				h++; 
			} else { 
				placeholderArray[i] = sortedIndexes_[j]; 
				j++; 
			} 
			
			i++;
		}
		if (h > mid) {
			for (int k=j; k<=high; k++) {
				placeholderArray[i] = sortedIndexes_[k]; 
				i++;
			}
		} else {
			for (int k=h; k<=mid; k++) {
				placeholderArray[i] = sortedIndexes_[k]; 
				i++;
			}
		}
		for (int k=low; k<=high; k++) {
			sortedIndexes_[k] = placeholderArray[k];
		}
	}

	public List<String> getLine(int line){
		return circularShifter_.getLine(sortedIndexes_[line]);
	}

	public String getLineAsString(int line){
		return circularShifter_.getLineAsString(sortedIndexes_[line]);
	}

	public int getNumberOfLines(){
		return circularShifter_.getNumberOfLines();
	}
}