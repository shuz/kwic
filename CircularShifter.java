import java.util.List;

/*
 * CircularShifter generates a list of circular shifted lines given a list of lines in LineStorage
 */
public class CircularShifter {
	private LineStorage circularShiftedLines_;

	public void circularShift(LineStorage lines){
		circularShiftedLines_ = new LineStorage();

		for(int i = 0; i < lines.getNumberOfLines(); i++){
			List<String> currentLine = lines.getLine(i);
			for(int startOfCircularIndex = 0; startOfCircularIndex < currentLine.size(); startOfCircularIndex++){
				circularShiftedLines_.addEmptyLine();
				int currentLineIndex = circularShiftedLines_.getNumberOfLines() - 1;
				
				boolean isKeyword = true;
				
				int currentCircularIndex = startOfCircularIndex;
				
				for(int l = 0; l < currentLine.size(); l++){
					if (currentCircularIndex == currentLine.size()){
						currentCircularIndex = 0;
					}
					if(isKeyword){
						circularShiftedLines_.addWord(currentLine.get(currentCircularIndex++).toUpperCase(), currentLineIndex);
						isKeyword = false;
					} else {
						circularShiftedLines_.addWord(currentLine.get(currentCircularIndex++), currentLineIndex);
					}
				}
			}
		}
	}

	public List<String> getLine(int line){
		return circularShiftedLines_.getLine(line);
	}

	public String getLineAsString(int line){
		return circularShiftedLines_.getLineAsString(line);
	}

	public int getNumberOfLines(){
		return circularShiftedLines_.getNumberOfLines();
	}
}
