/*
 * KWIC generates a list of circular shifted lines with the keywords in upper case and in 
 * ascending alphabetical order
 */
public class KWIC {	
	public static void main(String[] args){
		if(args.length != 2){
			System.err.println("Please type: java KWIC file_address_of_lines file_address_of_ignore_list");
			System.exit(1);
		}

		KWIC kwic = new KWIC();
		kwic.execute(args[0], args[1]);
	}

	public void execute(String lineFile, String ignoreFile){

		LineStorage lines = new LineStorage();
		Input input = new Input();
		Output output = new Output();
		CircularShifter circularShifter = new CircularShifter();
		AlphabetSorter alphabetSorter = new AlphabetSorter();


		input.setLineStorage(lineFile, lines);
		circularShifter.circularShift(lines);
		alphabetSorter.sort(circularShifter);
		output.print(ignoreFile, alphabetSorter);
	}
}
