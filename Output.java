import java.io.BufferedReader;
import java.io.FileNotFoundException;

import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

/* 
 * Output filters the sorted lines and prints the lines out
 */
public class Output {
	
	public void print(String ignoreFile, AlphabetSorter alphabetSorter){
		// generate the words to ignore list
		List<String> ignoredWords = new ArrayList<String>();
		try{
			BufferedReader reader = new BufferedReader(new FileReader(ignoreFile));

			String word = reader.readLine();

			while(word != null){
				ignoredWords.add(word);
				word = reader.readLine();
			}	      		      
			reader.close();
		} catch(FileNotFoundException outputError) {
			System.err.println("Output Error: " + ignoreFile + " file not found.");
			System.exit(1);
		} catch(IOException outputError) {
			System.err.println("Output Error: Could not read " + ignoreFile + " file.");
			System.exit(1);
		}

		for(int i = 0; i < alphabetSorter.getNumberOfLines(); i++){
			boolean isKeyword = true;
			for(int j = 0; j < ignoredWords.size(); j++){
				if(alphabetSorter.getLine(i).get(0).toLowerCase().equals(ignoredWords.get(j).toLowerCase())){
					isKeyword = false;
					break;
				}
			}
			if(isKeyword){
				System.out.println(alphabetSorter.getLineAsString(i));
			}
		}

		if(alphabetSorter.getNumberOfLines() == 0){				// empty text file
			System.out.println("Text file is empty!");
		}
	}
}
